import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import s from "./App.module.css";
import Counter from "./Counter/Counter";
import { CounterPageType } from "./redux/counterReducer";
import { AppStateType } from "./redux/redux-store";
import { SettingsWin } from "./SettingsWindow/SettingsWin";

function App() {
  let screen = useSelector<AppStateType, CounterPageType>(state => state.counter)

  return (
    <div className={s.AppWrapper}>
      <div>
        {
          screen.screenMode ? <Counter /> : <SettingsWin />
        }

      </div>
    </div>
  );
}

export default App;
