import React from "react";
import s from "./SettingsWin.module.css";
import { Buttons } from "../Buttons/Buttons";
import { InputValue } from "../Inputs/Inputs";
import { changeScreenMode, CounterPageType, setMaxValueAC, setMinValueAC, setScreenError, updateInputMaxValue, updateInputMinValue } from "../redux/counterReducer";
import { useDispatch, useSelector } from "react-redux";
import { AppStateType } from "../redux/redux-store";






export const SettingsWin: React.FC = () => {

  let settings = useSelector<AppStateType, CounterPageType>(state => state.counter)
  let dispatch = useDispatch();


  const onChangeHandlerMaxValue = (newValue: number) => {
    dispatch(updateInputMaxValue(newValue))
    if (settings.InputMinValue >= newValue) {
      dispatch(setScreenError(true))
    } else dispatch(setScreenError(false))
  }
  const onChangeHandlerMinValue = (newValue: number) => {
    dispatch(updateInputMinValue(newValue))
    if (settings.InputMaxValue <= newValue || newValue < 0) {
      dispatch(setScreenError(true))
    } else dispatch(setScreenError(false))
  }
  const sendValueToCounter = () => {
    dispatch(changeScreenMode(true))
    dispatch(setMaxValueAC())
    dispatch(setMinValueAC())
  };
  let error = settings.error



  return (
    <div className={s.wrapper}>
      <div className={s.CounterFirstWrapper} >
        <div className={s.displayWrapper}>
          <InputValue
            error={error}
            text={"Max value:"}
            inputValue={settings.InputMaxValue}
            onChange={(newValue: number) => { onChangeHandlerMaxValue(newValue) }}
          />
          <InputValue
            error={error}
            text={"Start value:"}
            inputValue={settings.InputMinValue}
            onChange={(newValue: number) => { onChangeHandlerMinValue(newValue) }}
          />
        </div>
      </div >

      <div className={s.buttonsWrapper}>
        <div className={s.buttons}>
          <div className={s.incButton}>
            <Buttons
              onClick={sendValueToCounter}
              text={"set"}
              disabled={error}
            />
          </div>
        </div>
      </div>

    </div>
  );
};
