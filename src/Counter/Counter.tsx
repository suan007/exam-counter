import React from "react";
import s from "./Counter.module.css";
import { Buttons } from "../Buttons/Buttons";
import { changeScreenMode, CounterPageType, onePlusAC, resetCounterAC } from "../redux/counterReducer";
import { useDispatch, useSelector } from "react-redux";
import { AppStateType } from "../redux/redux-store";



const Counter: React.FC = () => {

  let counter = useSelector<AppStateType, CounterPageType>(state => state.counter)
  let dispatch = useDispatch();
  const onePlus = () => {
    dispatch(onePlusAC())
  };
  const reset = () => {
    dispatch(resetCounterAC())
  };

  const isCounterEqualToMaxValue = counter.maxValue <= counter.counterValue;
  const checkHighesValue = isCounterEqualToMaxValue ? s.highestValue : s.countValue;


  const onSetClick = () => {
    dispatch(changeScreenMode(false))
  }
  return (
    <div className={s.wrapper}>
      <div className={s.CounterFirstWrapper}>
        <div className={s.counterWrapper}>
          <div className={checkHighesValue}> {counter.counterValue} </div>
        </div>
      </div>

      <div className={s.buttonsWrapper}>
        <div className={s.buttons}>
          <div className={s.incButton}>
            <Buttons
              onClick={onePlus}
              text={"inc"}
              disabled={isCounterEqualToMaxValue}
            />
          </div>
          <div className={s.resetButton}>
            <Buttons onClick={reset} text={"reset"} disabled={false} />
          </div>
          <div className={s.resetButton}>
            <Buttons onClick={onSetClick} text={"set"} disabled={isCounterEqualToMaxValue} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Counter;
