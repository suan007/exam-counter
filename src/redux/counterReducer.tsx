import React from "react";
import { act } from "react-dom/test-utils";

// types
export type OnePlusActionType = {
    type: "ADD_ONE_PLUS",
}
export type ResetCounterActionType = {
    type: "RESET_COUNTER",
}
export type SetMaxValueActionType = {
    type: "SET_MAX_VALUE",
}
export type SetMinValueActionType = {
    type: "SET_MIN_VALUE",
}

export type ChangeScreenModeActionType = {
    type: "CHANGE_SCREEN_MODE",
    newScreenValue: boolean
}

export type SetScreenErrorActionType = {
    type: "SET_SCREEN_ERROR",
    newErrorValue: boolean
}

export type UpdateInputMaxValueActionType = {
    type: "UPDATE_INPUT_MAX_VALUE",
    newInputMaxValue: number
}

export type UpdateInputMinValueActionType = {
    type: "UPDATE_INPUT_MIN_VALUE",
    newInputMinValue: number
}
export type ActionTypes =
    OnePlusActionType
    | ResetCounterActionType
    | SetMaxValueActionType
    | SetMinValueActionType
    | ChangeScreenModeActionType
    | SetScreenErrorActionType
    | UpdateInputMaxValueActionType
    | UpdateInputMinValueActionType


export type CounterPageType = typeof initalState;

const initalState = {
    maxValue: 10,
    minValue: 0,
    counterValue: 0,
    screenMode: true,
    error: false,
    InputMaxValue: 10,
    InputMinValue: 0,
}
//


export const counterPageReducer = (state: CounterPageType = initalState, action: ActionTypes): CounterPageType => {
    switch (action.type) {
        case "ADD_ONE_PLUS":
            let stateCopy = { ...state, counterValue: state.counterValue + 1 };
            return stateCopy
        case "RESET_COUNTER":
            return {
                ...state,
                counterValue: state.minValue
            }
        case "SET_MAX_VALUE":
            return {
                ...state,
                maxValue: state.InputMaxValue
            }
        case "SET_MIN_VALUE":
            return {
                ...state,
                counterValue: state.InputMinValue,
                minValue: state.InputMinValue
            }
        case "CHANGE_SCREEN_MODE":
            return {
                ...state,
                screenMode: action.newScreenValue
            }
        case "UPDATE_INPUT_MAX_VALUE":
            return {
                ...state,
                InputMaxValue: action.newInputMaxValue
            }
        case "UPDATE_INPUT_MIN_VALUE":
            return {
                ...state,
                InputMinValue: action.newInputMinValue
            }
        case "SET_SCREEN_ERROR":
            return {
                ...state,
                error: action.newErrorValue
            }
        default:
            return state
    }
}




// action creators

export const onePlusAC = (): OnePlusActionType => {
    return {
        type: "ADD_ONE_PLUS",

    } as const
}

export const resetCounterAC = (): ResetCounterActionType => {
    return {
        type: "RESET_COUNTER",


    } as const
}

export const setMaxValueAC = (): SetMaxValueActionType => {
    return {
        type: "SET_MAX_VALUE",
    } as const
}

export const setMinValueAC = (): SetMinValueActionType => {
    return {
        type: "SET_MIN_VALUE",
    } as const
}

export const changeScreenMode = (newScreenValue: boolean): ChangeScreenModeActionType => {
    return {
        type: "CHANGE_SCREEN_MODE",
        newScreenValue
    } as const
}

export const setScreenError = (newErrorValue: boolean): SetScreenErrorActionType => {
    return {
        type: "SET_SCREEN_ERROR",
        newErrorValue
    } as const
}

export const updateInputMaxValue = (newInputMaxValue: number): UpdateInputMaxValueActionType => {
    return {
        type: "UPDATE_INPUT_MAX_VALUE",
        newInputMaxValue
    } as const
}

export const updateInputMinValue = (newInputMinValue: number): UpdateInputMinValueActionType => {
    return {
        type: "UPDATE_INPUT_MIN_VALUE",
        newInputMinValue
    } as const
}

//thunk
