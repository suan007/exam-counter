import {
  changeScreenMode,
  counterPageReducer,
  CounterPageType,
  onePlusAC,
  resetCounterAC,
  setMaxValueAC,
  setMinValueAC,
  setScreenError,
} from './counterReducer'

let initalState: CounterPageType

beforeEach(() => {
  initalState = {
    maxValue: 10,
    minValue: 0,
    counterValue: 0,
    screenMode: true,
    error: false,
    InputMaxValue: 10,
    InputMinValue: 0,
  }
})

//1

test('counter should add one plus', () => {
  //
  const endState: CounterPageType = counterPageReducer(initalState, onePlusAC())
  //
  expect(endState.counterValue).toBe(endState.counterValue++)
  expect(endState.maxValue).toEqual(endState.maxValue)
})

//2

test('counter should reset to the minValue', () => {
  //
  const endState: CounterPageType = counterPageReducer(
    initalState,
    resetCounterAC()
  )
  //
  expect(endState.counterValue).toEqual(endState.minValue)
  expect(endState.maxValue).toEqual(endState.maxValue)
  expect(endState.minValue).toEqual(endState.minValue)
})

//3

test('maxValue should reset to the newMaxValue', () => {
  //
  const endState: CounterPageType = counterPageReducer(
    initalState,
    setMaxValueAC()
  )
  //
  expect(endState.counterValue).toEqual(endState.counterValue)
  expect(endState.minValue).toEqual(endState.minValue)
  expect(endState.maxValue).toBe(endState.InputMaxValue)
})

//4

test('minValue should reset to the newMinValue', () => {
  //
  const endState: CounterPageType = counterPageReducer(
    initalState,
    setMinValueAC()
  )
  //
  expect(endState.counterValue).toEqual(endState.counterValue)
  expect(endState.maxValue).toEqual(endState.maxValue)
  expect(endState.minValue).toBe(endState.InputMinValue)
})

//5

test('screen should change its boolean value', () => {
  //
  const endState: CounterPageType = counterPageReducer(
    initalState,
    changeScreenMode(false)
  )
  //
  expect(endState.screenMode).toBeFalsy
})

//6
test('error should change its boolean value', () => {
  //
  const endState: CounterPageType = counterPageReducer(
    initalState,
    setScreenError(false)
  )
  //
  expect(endState.error).toBeFalsy
})
