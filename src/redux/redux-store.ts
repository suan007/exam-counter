import { loadState, saveState } from './../utils/localStorage'
import { applyMiddleware, combineReducers, createStore } from 'redux'
import thunk from 'redux-thunk'
import { counterPageReducer } from './counterReducer'

let rootReducer = combineReducers({
  counter: counterPageReducer,
})

export let store = createStore(rootReducer, loadState(), applyMiddleware(thunk))

store.subscribe(() => {
  saveState({
    counter: store.getState().counter,
  })
})

type RootReducerType = typeof rootReducer

export type AppStateType = ReturnType<RootReducerType>
