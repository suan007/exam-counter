import React, { ChangeEvent, useState } from "react";
import s from "./Inputs.module.css";

type InputPropsType = {
  error: boolean | undefined
  text: string;
  inputValue: number;
  onChange: (newValue: number) => void;
};

export const InputValue: React.FC<InputPropsType> = (props) => {
  const onChangeHandler = (e: ChangeEvent<HTMLInputElement>) => {
    let value = e.currentTarget.valueAsNumber;
    props.onChange(value);
  };
 
  return (
    <div className={s.wrapper}>
      <div className={s.text}>{props.text}</div>
      <div className={s.inputs}>
        <input
          className={props.error ? s.error : s.input}
          onChange={onChangeHandler}
          type="number"
          value={props.inputValue}
          max="15"
          min="-15"
        />
      </div>
    </div>
  );
};
