import React from "react";
import s from "./Buttons.module.css";

type ButtonsType = {
  onClick: () => void;
  text: string;
  disabled: boolean;
};

export const Buttons: React.FC<ButtonsType> = (props) => {
  const onClickHandler = () => {
    props.onClick();
  };
  const buttonStyle = props.disabled ? s.disButton : s.button;
  return (
    <div className={s.onePlus}>
      <button
        className={buttonStyle}
        disabled={props.disabled}
        onClick={onClickHandler}
      >
        {props.text}
      </button>
    </div>
  );
};
